package com.luxoft.akkalabs.day1.wikipedia;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.dispatch.Futures;
import com.luxoft.akkalabs.clients.twitter.TwitterClient;
import com.luxoft.akkalabs.clients.twitter.TwitterClients;
import com.luxoft.akkalabs.day1.futures.CollectTweets;
import com.luxoft.akkalabs.day1.futures.domain.TweetsResult;
import scala.concurrent.Future;

public class GrabWikipediaLinksFromTweets {

    public static void main(String[] args) {
        ActorSystem system = ActorSystem.create("GrabWikipediaLinksFromTweets");

        ActorRef linksActor = system.actorOf(Props.create(WikipediaActor.class));
        ActorRef tweetsActor = system.actorOf(Props.create(TweetLinksActor.class, linksActor));

        TwitterClients.start(system, tweetsActor, "wikipedia");
    }
}
