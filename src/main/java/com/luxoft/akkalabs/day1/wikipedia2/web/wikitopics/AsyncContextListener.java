package com.luxoft.akkalabs.day1.wikipedia2.web.wikitopics;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;

import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import java.io.IOException;

/**
 * Created by User on 08.12.2014.
 */
public class AsyncContextListener implements AsyncListener {

    private String streamId;
    private ActorSelection actorSelection;

    public AsyncContextListener(String streamId, ActorSelection actorSelection) {
        this.streamId = streamId;
        this.actorSelection = actorSelection;
    }

    private void unregister() {
        actorSelection.tell(new Unregister(streamId), null);
    }

    @Override
    public void onComplete(AsyncEvent asyncEvent) throws IOException {
        unregister();
    }

    @Override
    public void onTimeout(AsyncEvent asyncEvent) throws IOException {
        unregister();
        asyncEvent.getAsyncContext().complete();
    }

    @Override
    public void onError(AsyncEvent asyncEvent) throws IOException {
        unregister();
    }

    @Override
    public void onStartAsync(AsyncEvent asyncEvent) throws IOException {

    }
}
