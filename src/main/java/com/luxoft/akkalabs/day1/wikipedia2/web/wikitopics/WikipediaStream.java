package com.luxoft.akkalabs.day1.wikipedia2.web.wikitopics;

import akka.actor.ActorSelection;
import akka.actor.ActorSystem;

import java.io.IOException;
import java.util.UUID;
import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(asyncSupported = true, urlPatterns = {"/day1/wikitopics"})
public class WikipediaStream extends HttpServlet {
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/event-stream");
        resp.setCharacterEncoding("UTF-8");
        AsyncContext context = req.startAsync();
        context.setTimeout(240000);

        ActorSystem system = (ActorSystem) getServletContext().getAttribute("actorSystem");
        ActorSelection actorSelection = system.actorSelection("/user/connections");

        String streamId = UUID.randomUUID().toString();
        WikiListenerImpl wikiListener = new WikiListenerImpl(context, streamId);
        actorSelection.tell(new Register(wikiListener), null);
        AsyncContextListener asyncContextListener = new AsyncContextListener(streamId, actorSelection);
        context.addListener(asyncContextListener);
    }

}
