package com.luxoft.akkalabs.day1.wikipedia2.web.wikitopics;

import com.luxoft.akkalabs.clients.wikipedia.WikipediaPage;

import javax.servlet.AsyncContext;
import java.io.IOException;
import java.io.Writer;

/**
 * Created by User on 08.12.2014.
 */
public class WikiListenerImpl implements WikipediaListener {

    private AsyncContext context;

    private String id;

    private Writer out;

    public WikiListenerImpl(AsyncContext context, String id) {
        this.context = context;
        this.id = id;
    }

    @Override
    public void deliver(WikipediaPage page) throws NotDeliveredException {
        try {
            if (out == null) {
                out = context.getResponse().getWriter();
            }

            String data = page.toJSONString();
            String eventId = Long.toString(System.currentTimeMillis());
            String[] lines = data.split("\n");
            out.append("id: ").append(eventId).append('\n');
            for (String line : lines) {
                out.append("data: ").append(line).append('\n');
            }
            out.append("\n\n");
            context.getResponse().flushBuffer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getStreamId() {
        return id;
    }

    @Override
    public void close() {

    }
}
