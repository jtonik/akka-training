package com.luxoft.akkalabs.day1.languages;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.dispatch.Futures;
import akka.dispatch.OnComplete;
import akka.dispatch.OnSuccess;
import akka.pattern.Patterns;
import akka.util.Timeout;
import com.luxoft.akkalabs.day1.futures.CollectTweets;
import com.luxoft.akkalabs.day1.futures.TweetResultMapper;
import com.luxoft.akkalabs.day1.futures.actor.LanguagesCounterActor;
import com.luxoft.akkalabs.day1.futures.domain.TweetsLangResult;
import com.luxoft.akkalabs.day1.futures.domain.TweetsResult;
import scala.concurrent.Future;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class PopularLanguages {

    public static void main(String[] args) throws Exception {
        ActorSystem system = ActorSystem.create("PopularLanguages");

        ActorRef actor = system.actorOf(Props.create(LanguagesCounterActor.class));

        final int numTweets = 10;

        List<String> keywords = Arrays.asList("Google", "Apple", "Android", "iPhone", "Lady Gaga");

        for (String keyword : keywords) {
            Future<TweetsLangResult> f = Futures.future(new CollectTweets(keyword, numTweets, system), system.dispatcher()).
                    map( new TweetResultMapper(), system.dispatcher());
//                    onSuccess( new SendToActor(actor), system.dispatcher());

            Patterns.pipe(f, system.dispatcher()).to(actor);
        }

        Timeout timeout = Timeout.apply(1, TimeUnit.SECONDS);
        for (int i = 0; i < 30; i++) {
            Thread.sleep(1000);
            Future<Object> future = Patterns.ask(actor, "get", timeout);
            future.onComplete(new OnComplete<Object>() {
                @SuppressWarnings("unchecked")
                @Override
                public void onComplete(Throwable throwable, Object o) throws Throwable {
                    Map<String, Integer> result = (Map<String, Integer>) o;
                    System.out.println(">> langs: " + result);
                }
            }, system.dispatcher());
        }

//        Thread.sleep(30_000);

        system.shutdown();
    }

    private static class SendToActor extends OnSuccess<TweetsLangResult> {

        private ActorRef actor;

        private SendToActor(ActorRef actor) {
            this.actor = actor;
        }

        @Override
        public void onSuccess(TweetsLangResult tweetsLangResult) throws Throwable {
            actor.tell(tweetsLangResult, null);
        }
    }
}
