package com.luxoft.akkalabs.day1.futures.domain;

import com.luxoft.akkalabs.clients.twitter.TweetObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 08.12.2014.
 */
public class TweetsLangResult {

    private String keyword;

    private Map<String, Integer> languages;

    public TweetsLangResult(String keyword) {
        this.keyword = keyword;
        languages = new HashMap<>();
    }

    public Map<String, Integer> getLanguages() {
        return languages;
    }

    public void addTweet(TweetObject tweet) {
        if (languages.get(tweet.getLanguage()) != null) {
            languages.put(tweet.getLanguage(), languages.get(tweet.getLanguage()) + 1);
        }
        else {
            languages.put(tweet.getLanguage(), 1);
        }
    }

    @Override
    public String toString() {
        return "TweetsLangResult{" +
                "keyword='" + keyword + '\'' +
                ", languages=" + languages +
                '}';
    }
}
