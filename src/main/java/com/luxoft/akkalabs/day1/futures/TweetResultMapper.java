package com.luxoft.akkalabs.day1.futures;

import akka.dispatch.Mapper;
import com.luxoft.akkalabs.clients.twitter.TweetObject;
import com.luxoft.akkalabs.day1.futures.domain.TweetsLangResult;
import com.luxoft.akkalabs.day1.futures.domain.TweetsResult;

/**
* Created by User on 08.12.2014.
*/
public class TweetResultMapper extends Mapper<TweetsResult, TweetsLangResult> {
    @Override
    public TweetsLangResult apply(TweetsResult tweetsResult) {
        TweetsLangResult langResult = new TweetsLangResult(tweetsResult.getKeyword());
        for (TweetObject tweet : tweetsResult.getTweets()) {
//                System.out.println("-------------------------------------------");
//                System.out.println(tweet.getText());
            langResult.addTweet(tweet);
        }
        return langResult;
    }
}
