package com.luxoft.akkalabs.day1.futures.domain;

import com.luxoft.akkalabs.clients.twitter.TweetObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 08.12.2014.
 */
public class TweetsResult {

    private  String keyword;

    private List<TweetObject> tweets;

    public TweetsResult(String keyword) {
        this.keyword = keyword;
        this.tweets = new ArrayList<>();
    }

    public void addTweet(TweetObject tweet) {
        tweets.add(tweet);
    }

    public String getKeyword() {
        return keyword;
    }

    public List<TweetObject> getTweets() {
        return tweets;
    }
}
