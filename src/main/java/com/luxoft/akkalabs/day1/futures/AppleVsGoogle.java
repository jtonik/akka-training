package com.luxoft.akkalabs.day1.futures;

import akka.actor.ActorSystem;
import akka.dispatch.Futures;
import akka.dispatch.OnSuccess;
import com.luxoft.akkalabs.day1.futures.domain.TweetsLangResult;
import com.luxoft.akkalabs.day1.futures.domain.TweetsResult;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AppleVsGoogle {

    public static final String SYSTEM_NAME = "AppleVsGoogle";

    public static void main(String[] args) throws Exception {

        ActorSystem system = ActorSystem.create(SYSTEM_NAME);

        final int numTweets = 10;

        Future<TweetsResult> futureAppleResult = Futures.future(new CollectTweets("Apple", numTweets, system), system.dispatcher());
        Future<TweetsResult> futureGoogleResult = Futures.future(new CollectTweets("Google", numTweets, system), system.dispatcher());

        Future<TweetsLangResult> futureAppleLangs = futureAppleResult.map(new TweetResultMapper(), system.dispatcher());
        Future<TweetsLangResult> futureGoogleLangs = futureGoogleResult.map(new TweetResultMapper(), system.dispatcher());

        List<Future<TweetsLangResult>> combinedResult = Arrays.asList(futureAppleLangs, futureGoogleLangs);

        Future<Iterable<TweetsLangResult>> result = Futures.sequence(combinedResult, system.dispatcher());

        result.onSuccess(new OnSuccess<Iterable<TweetsLangResult>>() {
            @Override
            public void onSuccess(Iterable<TweetsLangResult> tweetsLangResults) throws Throwable {
                Iterator<TweetsLangResult> langsResult = tweetsLangResults.iterator();

                while (langsResult.hasNext()) {
                    System.out.println(langsResult.next());
                }
            }
        }, system.dispatcher());

        Await.result(result, Duration.create(60, TimeUnit.SECONDS));
        system.shutdown();
    }

}
