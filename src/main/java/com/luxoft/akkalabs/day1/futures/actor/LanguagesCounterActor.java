package com.luxoft.akkalabs.day1.futures.actor;

import akka.actor.UntypedActor;
import com.luxoft.akkalabs.day1.futures.domain.TweetsLangResult;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 08.12.2014.
 */
public class LanguagesCounterActor extends UntypedActor {

    private final Map<String, Integer> languages;

    public LanguagesCounterActor() {
        languages = new HashMap<>();
    }

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof TweetsLangResult) {
            Map<String, Integer> langResult = ((TweetsLangResult)o).getLanguages();

            for (Map.Entry<String, Integer> lang : langResult.entrySet()) {
                Integer occurrences = languages.get(lang.getKey());
                occurrences = occurrences == null ? lang.getValue() : occurrences + lang.getValue();
                languages.put(lang.getKey(), occurrences);
            }
        }
        else if ("get".equals(o)) {
            getSender().tell(new HashMap<>(languages), self());
        }
    }
}
