package com.luxoft.akkalabs.day1.futures;

import akka.actor.ActorSystem;
import com.luxoft.akkalabs.clients.twitter.QueueTwitterClient;
import com.luxoft.akkalabs.clients.twitter.TweetObject;
import com.luxoft.akkalabs.clients.twitter.TwitterClients;
import com.luxoft.akkalabs.day1.futures.domain.TweetsResult;

import java.util.concurrent.Callable;

/**
 * Created by User on 08.12.2014.
 */
public class CollectTweets implements Callable<TweetsResult> {

    private final int numTweets;

    private final ActorSystem actorSystem;

    private final String keyword;

    public CollectTweets(String keyword, int numTweets, ActorSystem actorSystem) {
        this.keyword = keyword;
        this.actorSystem = actorSystem;
        this.numTweets = numTweets;
    }

    @Override
    public TweetsResult call() throws Exception {
        TweetsResult result = new TweetsResult(keyword);
        QueueTwitterClient client = TwitterClients.start(actorSystem, keyword);
        for (int i = 0; i < numTweets; i++) {
//            if (client.hasNext()) {
                TweetObject tweet = client.next();
                result.addTweet(tweet);
//            } else {
//                System.out.println("Not enough tweets");
//                break;
//            }
        }
        client.stop();
        return result;
    }

}
