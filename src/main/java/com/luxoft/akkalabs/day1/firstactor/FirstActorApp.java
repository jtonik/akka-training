package com.luxoft.akkalabs.day1.firstactor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

public class FirstActorApp {

    public static void main(String[] args) throws Exception {
        ActorSystem system = ActorSystem.create("system");

        ActorRef actor = system.actorOf(Props.create(PingActor.class));
        actor.tell("ping", null);
        Thread.sleep(100);
        actor.tell("ping", null);
        actor.tell("ping", null);
        actor.tell("ping", null);

        Thread.sleep(2000);
        system.shutdown();
    }

}
