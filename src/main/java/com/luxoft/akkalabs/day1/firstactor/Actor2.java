package com.luxoft.akkalabs.day1.firstactor;

import akka.actor.UntypedActor;

/**
 * Created by User on 08.12.2014.
 */
public class Actor2 extends UntypedActor {

    private String param;

    public Actor2() {
    }

    public Actor2(String param) {
        this.param = param;
        System.out.println(param);
    }

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof String) {
            System.out.println(o);
        }
    }
}
