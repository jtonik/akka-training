package com.luxoft.akkalabs.day1.firstactor;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;

import java.util.Random;

/**
 * Created by User on 08.12.2014.
 */
public class PingActor extends UntypedActor {

    private Random rand = new Random();

    @Override
    public void onReceive(Object o) throws Exception {

//        Thread.sleep(rand.nextInt(100));

        long millis = System.currentTimeMillis();

        if ("ping".equals(o)) {
            System.out.println("ping@" + millis);
        }

        ActorRef actor2 = getContext().system().actorOf(Props.create(Actor2.class, "a2"));
        actor2.tell("How are you?", self());

    }
}
