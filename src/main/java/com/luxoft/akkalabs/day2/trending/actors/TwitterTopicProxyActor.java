package com.luxoft.akkalabs.day2.trending.actors;

import akka.actor.*;
import com.luxoft.akkalabs.clients.twitter.TweetObject;
import com.luxoft.akkalabs.day2.topics.actors.TwitterTopicActor;
import com.luxoft.akkalabs.day2.topics.messages.SubscribeToTopic;
import com.luxoft.akkalabs.day2.topics.messages.UnsubscribeFromTopic;

import java.util.HashSet;
import java.util.Set;

public class TwitterTopicProxyActor extends UntypedActor {

    private final String keyword;
    private ActorRef topicActor;
    private ActorSelection trending;
    private Set<ActorRef> subscribers;

    public TwitterTopicProxyActor(String keyword) {
        this.keyword = keyword;
        subscribers = new HashSet<>();
    }

    @Override
    public void preStart() throws Exception {
        topicActor = getContext().actorOf(Props.create(TwitterTopicActor.class, keyword));
        context().watch(topicActor);
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (!getSender().equals(topicActor)) {
            if (message instanceof SubscribeToTopic) {
                if (subscribers.isEmpty()) {
                    topicActor.tell(new SubscribeToTopic(keyword), self());
                }
                subscribers.add(getSender());
            }
            else if (message instanceof UnsubscribeFromTopic) {
                subscribers.remove(getSender());
                if (subscribers.isEmpty()) {
                    topicActor.tell(new UnsubscribeFromTopic(keyword), self());
                }
            }
            else {
                topicActor.forward(message, getContext());
            }
        }
        else { //sender = topicActor
            if (message instanceof TweetObject) {
                for (ActorRef subscriber : subscribers) {
                    subscriber.tell(message, self());
                }
                getTrending().tell(message, self());
            }
            else if (message instanceof Terminated) {
                getContext().stop(self());
            }
            else {
                getContext().parent().forward(message, getContext());
            }
        }
    }

    private ActorSelection getTrending() {
        if (trending == null) {
            trending = getContext().actorSelection("/user/trending");
        }
        return trending;
    }
}
