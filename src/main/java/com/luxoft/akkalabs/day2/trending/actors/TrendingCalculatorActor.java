package com.luxoft.akkalabs.day2.trending.actors;

import akka.actor.ActorSelection;
import akka.actor.UntypedActor;
import com.luxoft.akkalabs.clients.twitter.TweetObject;
import com.luxoft.akkalabs.day2.sessions.messages.OutgoingBroadcast;
import com.luxoft.akkalabs.day2.trending.messages.CurrentTrending;
import com.luxoft.akkalabs.day2.trending.messages.UpvoteTrending;
import scala.concurrent.duration.FiniteDuration;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class TrendingCalculatorActor extends UntypedActor {

    private final Object PING = new Object();

    private final int NUM_TOP_WORDS = 10;

    private Map<String, Integer> words = new HashMap<>();

    private ActorSelection subscribers;
    
    @Override
    public void preStart() throws Exception {
        FiniteDuration oneSecond = FiniteDuration.create(1, TimeUnit.SECONDS);
        context().system().scheduler().schedule(
                oneSecond, oneSecond, self(), PING, context().dispatcher(), self());
    }

    @Override
    public void postStop() throws Exception {
        super.postStop();
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message == PING) {
            getSubscribers().tell(new OutgoingBroadcast(getTopWords()), self());
        }
        else if (message instanceof TweetObject) {
            parseTweet((TweetObject) message);
        }
        else if (message instanceof UpvoteTrending) {
            voteForWord(((UpvoteTrending) message).getKeyword(), 5);
        }
    }

    private ActorSelection getSubscribers() {
        if (subscribers == null) {
            subscribers = getContext().actorSelection("/user/sessions");
        }
        return subscribers;
    }

    private void parseTweet(TweetObject tweet) {
        Set<String> urls = new HashSet<>(tweet.getUrls());
        Scanner scanner = new Scanner(tweet.getText());
        scanner.useDelimiter("[\\s]+");
        while (scanner.hasNext()) {
            String word = scanner.next();
            if (!urls.contains(word) && word.length() > 3 && !word.startsWith("http")) {
                voteForWord(word, 1);
            }
        }
    }

    private void voteForWord(String word, int scores) {
        Integer rating = words.get(word);
        if (rating == null) {
            rating = scores;
        }
        else {
            rating += scores;
        }
        words.put(word, rating);
    }

    private CurrentTrending getTopWords() {
        TreeSet<Map.Entry<String, Integer>> top = new TreeSet<>(new Comparator<Map.Entry<String, Integer>>(){

            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o2.getValue() - o1.getValue();
            }
        });

        top.addAll(words.entrySet());
        List<String> topWords = new ArrayList<>(NUM_TOP_WORDS);
        int i = 0;
        for (Map.Entry<String, Integer> entry : top) {
            if (i < NUM_TOP_WORDS) {
                topWords.add(entry.getKey()); //TODO show rating on page
            }
            else {
                break;
            }
        }
        return new CurrentTrending(topWords);
    }
}
