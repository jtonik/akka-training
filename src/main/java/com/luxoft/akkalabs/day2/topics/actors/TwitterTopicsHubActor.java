package com.luxoft.akkalabs.day2.topics.actors;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.luxoft.akkalabs.day2.topics.messages.StopTopic;
import com.luxoft.akkalabs.day2.topics.messages.SubscribeToTopic;
import com.luxoft.akkalabs.day2.topics.messages.TopicIsEmpty;
import com.luxoft.akkalabs.day2.topics.messages.UnsubscribeFromTopic;

public class TwitterTopicsHubActor extends UntypedActor {

    private final Class<? extends UntypedActor> topicClass;

    public TwitterTopicsHubActor(Class<? extends UntypedActor> topicClass) {
        this.topicClass = topicClass;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof SubscribeToTopic) {
            SubscribeToTopic stt = (SubscribeToTopic) message;
            System.out.println("Subscribing to '" + stt.getKeyword() + "'");
            ActorRef topicActor = getTopicActor(stt.getKeyword());
            if (topicActor == null) {
                topicActor = getContext().actorOf(Props.create(topicClass, stt.getKeyword()));
            }
            topicActor.forward(message, getContext());
        }
        else if (message instanceof UnsubscribeFromTopic) {
            ActorRef topicActor = getTopicActor(((UnsubscribeFromTopic) message).getKeyword());
            System.out.println("Unsubscribing from '" + ((UnsubscribeFromTopic) message).getKeyword() + "'");
            if (topicActor != null) {
                topicActor.forward(message, context());
            }
        }
        else if (message instanceof TopicIsEmpty) {
            ActorRef topicActor = getTopicActor(((TopicIsEmpty) message).getKeyword());
            if (topicActor != null) {
                topicActor.tell(StopTopic.getInstance(), self());
                getContext().stop(topicActor);
            }
        }
    }

    private ActorRef getTopicActor (String keyword) {
        return getContext().getChild(keyword);
    }
}
