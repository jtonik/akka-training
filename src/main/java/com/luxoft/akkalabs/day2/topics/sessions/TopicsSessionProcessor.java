package com.luxoft.akkalabs.day2.topics.sessions;

import akka.actor.ActorContext;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import com.luxoft.akkalabs.clients.twitter.TweetObject;
import com.luxoft.akkalabs.day2.sessions.SessionProcessor;
import com.luxoft.akkalabs.day2.topics.messages.SubscribeToTopic;
import com.luxoft.akkalabs.day2.topics.messages.UnsubscribeFromTopic;

import java.io.IOException;
import javax.websocket.Session;

public class TopicsSessionProcessor implements SessionProcessor {

    private String sessionId;
    private ActorContext context;
    private Session session;

    private ActorSelection topicsActor;

    @Override
    public void started(String sessionId, ActorContext context, Session session) {
        this.sessionId = sessionId;
        this.context = context;
        this.session = session;
    }

    @Override
    public void stopped() {
    }

    @Override
    public void incoming(String message) {
//        System.out.println("incomming <- " + message);
        String[] tokens = message.split(" ", 2);
        if (tokens.length == 2) {
            switch (tokens[0]) {
                case "subscribe":
                        getTopicsActor().tell(new SubscribeToTopic(tokens[1]), context.self());
                    break;
                case "unsubscribe":
                    getTopicsActor().tell(new UnsubscribeFromTopic(tokens[1]), context.self());
                    break;
            }
        }
    }

    @Override
    public void outgoing(Object message) throws IOException {
//        System.out.println("outgoing -> " + message);
        if (message instanceof TweetObject) {
            TweetObject tweet = (TweetObject) message;
            session.getBasicRemote().sendText("tweet " + tweet.getText());
        }
    }

    private ActorSelection getTopicsActor() {
        if (topicsActor == null) {
            topicsActor = context.system().actorSelection("/user/topics");
        }
        return topicsActor;
    }
}
