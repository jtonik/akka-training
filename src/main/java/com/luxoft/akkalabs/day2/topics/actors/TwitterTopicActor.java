package com.luxoft.akkalabs.day2.topics.actors;

import akka.actor.ActorRef;
import com.luxoft.akkalabs.clients.twitter.TwitterClient;
import com.luxoft.akkalabs.clients.twitter.TwitterClients;
import com.luxoft.akkalabs.day2.topics.messages.TopicIsEmpty;
import com.luxoft.akkalabs.day2.topics.messages.UnsubscribeFromTopic;
import com.luxoft.akkalabs.day2.topics.messages.StopTopic;
import com.luxoft.akkalabs.day2.topics.messages.SubscribeToTopic;
import akka.actor.UntypedActor;
import com.luxoft.akkalabs.clients.twitter.TweetObject;

import java.util.HashSet;
import java.util.Set;

public class TwitterTopicActor extends UntypedActor {

    private final String keyword;
    
    private TwitterClient client;

    private Set<ActorRef> subscribers;

    public TwitterTopicActor(String keyword) {
        this.keyword = keyword;
        subscribers = new HashSet<>();
    }

    @Override
    public void preStart() throws Exception {
        client = TwitterClients.start(getContext().system(), getSelf(), keyword);
    }

    @Override
    public void postStop() throws Exception {
        client.stop();
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof TweetObject) {
            for (ActorRef subscriber : subscribers) {
                subscriber.forward(message, getContext());
            }
        }
        else if (message instanceof SubscribeToTopic) {
            subscribers.add(getSender());
        }
        else if (message instanceof UnsubscribeFromTopic) {
            subscribers.remove(getSender());
            if (subscribers.isEmpty()) {
                getContext().parent().tell(new TopicIsEmpty(keyword), self());
            }
        }
        else if (message instanceof StopTopic) {
            if (!subscribers.isEmpty()) {
                for (ActorRef subscriber : subscribers) {
                    getContext().parent().tell(new SubscribeToTopic(keyword), subscriber);
                }
            }
            context().stop(self());
        }
    }
}
