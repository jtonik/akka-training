package com.luxoft.akkalabs.day2.sessions.actors;

import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.luxoft.akkalabs.day2.sessions.SessionProcessor;
import com.luxoft.akkalabs.day2.sessions.messages.OutgoingBroadcast;
import com.luxoft.akkalabs.day2.sessions.messages.OutgoingToSession;
import com.luxoft.akkalabs.day2.sessions.messages.RegisterSession;
import com.luxoft.akkalabs.day2.sessions.messages.UnregisterSession;

public class SessionsHubActor extends UntypedActor {

    private final Class<? extends SessionProcessor> processorClass;

    public SessionsHubActor(Class<? extends SessionProcessor> processor) {
        this.processorClass = processor;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof RegisterSession) {
            RegisterSession rs = (RegisterSession) message;
            SessionProcessor sessionProcessor = processorClass.newInstance();
            getContext().actorOf(
                    Props.create(SessionActor.class, rs.getSessionId(), sessionProcessor, rs.getSession()),
                    rs.getSessionId());
        }
        else if (message instanceof UnregisterSession) {
            UnregisterSession uns = (UnregisterSession) message;
            getContext().stop(getContext().getChild(uns.getSessionId()));
        }
        else if (message instanceof OutgoingToSession) {
            ActorRef sessionActor = getContext().getChild(((OutgoingToSession) message).getSessionId());
            sessionActor.forward(message, getContext());
        }
        else if (message instanceof OutgoingBroadcast) {
            Iterable<ActorRef> children = getContext().getChildren();
            for (ActorRef child : children) {
                child.forward(message, getContext());
            }
        }
    }
}
