package com.luxoft.akkalabs.day2.sessions.actors;

import akka.actor.UntypedActor;
import com.luxoft.akkalabs.day2.sessions.SessionProcessor;
import com.luxoft.akkalabs.day2.sessions.messages.Incoming;
import com.luxoft.akkalabs.day2.sessions.messages.Outgoing;

import javax.websocket.Session;

import static javax.websocket.MessageHandler.Whole;

public class SessionActor extends UntypedActor {

    private final String sessionId;
    private final SessionProcessor sessionProcessor;
    private final Session session;

    public SessionActor(String sessionId, SessionProcessor sessionProcessor, Session session) {
        this.sessionId = sessionId;
        this.sessionProcessor = sessionProcessor;
        this.session = session;
    }

    @Override
    public void preStart() throws Exception {
        Whole<String> listener = new Whole<String>() {
            @Override
            public void onMessage(String message) {
                self().tell(new Incoming(message), self());
            }
        };

        session.addMessageHandler(listener);

        sessionProcessor.started(sessionId, context(), session);
    }

    @Override
    public void postStop() throws Exception {
        sessionProcessor.stopped();
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof Incoming) {
            sessionProcessor.incoming(((Incoming) message).getMessage());
        } else if (message instanceof Outgoing) {
            sessionProcessor.outgoing(((Outgoing) message).getMessage());
        } else {
            sessionProcessor.outgoing(message);
        }
    }
}
